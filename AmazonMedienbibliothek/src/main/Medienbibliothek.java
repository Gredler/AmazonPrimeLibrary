package main;
import java.util.ArrayList;

public class Medienbibliothek 
{
	
	private ArrayList<Medium> medienListe;

	public Medienbibliothek() 
	{
		medienListe = new ArrayList<>();
	}

	public ArrayList<Medium> getMedienListe() 
	{	
		return medienListe;
	}

	public void setMedienListe(ArrayList<Medium> medienListe) 
	{
		this.medienListe = medienListe;
	}
	
	public void addMedium(Medium m) 
	{
		medienListe.add(m);
	}
	
	public String allesMedienAusgeben() 
	{
		String ausgabe = "";
		for(Medium m : medienListe) 
		{
			ausgabe += m.toString();
			ausgabe += "\n";
		}
		return ausgabe;
	}
	
	public String mediumSuchen(String suchbegriff) 
	{
		for(Medium m : medienListe) 
		{
			if(m.getTitel().equals(suchbegriff)) 
			{
				return m.toString();
			}
		}
		return "Medium nicht gefunden!";
	}
}
