package main;

public class Medium 
{

	private String titel;
	private	String kommentar;
	private String erscheinungsdatum;
	private Genre genre;
	
	public Medium(String titel, String kommentar, String erscheinungsdatum, Genre genre) 
	{

		this.titel = titel;
		this.kommentar = kommentar;
		this.erscheinungsdatum = erscheinungsdatum;
		this.genre = genre;
		
		
	}

	public String getTitel() 
	{
		return titel;
	}

	public void setTitel(String titel) 
	{
		this.titel = titel;
	}

	public String getKommentar() 
	{
		return kommentar;
	}

	public void setKommentar(String kommentar) 
	{
		this.kommentar = kommentar;
	}

	public String getErscheinungsdatum() {
		return erscheinungsdatum;
	}

	public void setErscheinungsdatum(String erscheinungsdatum) 
	{
		this.erscheinungsdatum = erscheinungsdatum;
	}

	public Genre getGenre() 
	{
		return genre;
	}

	public void setGenre(Genre genre) 
	{
		this.genre = genre;
	}
	
	public String toString() 
	{
		return "[Medium] Titel: " + getTitel() + "Kommentar: " + getKommentar() + "Erscheinung: " + getErscheinungsdatum() + "Genre: " + getGenre() + "\n" ;
	}
}
